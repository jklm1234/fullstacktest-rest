const app = require('express')();
var serveStatic = require('serve-static')

const server = require('http').createServer(app);
const io = require('socket.io')(server);
var dynamoDB = require("./services/dynamoDbService");
io.on('connection', (socket) => {
    console.log("socket connected", io.sockets.server.engine.clientsCount);
    //StartSendingData(socket);
});


server.listen(process.env.PORT || 3000);

// const cache_options = {
//     dotfiles: 'ignore',
//     etag: true,
//     maxAge: config.cacheTime,
//     extensions: ['js', 'css', 'jpg', 'png', 'jpeg', 'html'],
//     index: false,
//     redirect: false,
//     // expires: new Date(Date.now() + 86400000 + 86400000),
//     setHeaders: function (res, path, stat) {
//         // res.set('x-timestamp', Date.now())
//         res.set('Expires', new Date(Date.now() + 86400000 + 86400000));
//     }
// }

app.use(serveStatic(__dirname + '/dist'));





















var SendDataToClient = function (data, cliendId) {
    io.emit("heartrate", data);
}
var StartSendingData = function (socket) {
    setInterval(() => {
        if (io.sockets.server.engine.clientsCount === 0) {
            console.log("No clients found");
        } else {
            let randomNo = GetRandomNo();
            data = {
                randomNo: randomNo,
                time: Date.now()
            }
            SendDataToClient(data, socket);
            dynamoDB.insert(data, function (error, data) {
                if (error) {
                    console.log("error while inserting to dynamodb ", error);
                }
            })
        }
    }, 1000)
}

var GetRandomNo = function () {
    return parseInt(Math.random() * 10);
}

StartSendingData();